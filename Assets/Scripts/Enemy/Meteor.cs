﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using  UnityEngine.UI;

public class Meteor : MonoBehaviour
{
    public float speed = 1f;

    Vector3 objStart;
    void Start()
    {
        objStart = transform.position;
    }
    void FixedUpdate()
    {
        transform.Translate (Vector3.down * (speed * Time.deltaTime));
    }
    void OnBecameInvisible()
    {
        transform.position = objStart;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Players"))
        {
            Destroy(other.gameObject);
        }
    }
}
