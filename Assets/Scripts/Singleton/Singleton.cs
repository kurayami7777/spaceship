﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static bool isShuttingDown = false;
    private static object lockObject = new object();
    private static T instance;


    public static T Instance
    {
        get
        {
            if (isShuttingDown)
            {
                return null;
            }
            lock (lockObject)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();

                    if (instance == null)
                    {
                        var singleton = new GameObject();
                        instance = singleton.AddComponent<T>();
                        singleton.name = instance.GetType().Name;
                        DontDestroyOnLoad(singleton);
                    }
                }

                return instance;
            }
        }
    }
}