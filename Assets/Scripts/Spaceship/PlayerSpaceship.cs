using System;
using System.Collections;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip PlayerDeath;
        [SerializeField] private float PlayerDeathVolume = 0.5f;
        private bool ratefire = true;
        

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet, bullet2);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(gunPosition.up * 20 , ForceMode2D.Impulse);
            /*var playerbullet2 = Instantiate(bullet2, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);*/
        }

        public override void Fire2()
        {
            StartCoroutine(Firerate2());
        }

        private IEnumerator Firerate2()
        {
            if (ratefire)
            {
                ratefire = false;
                var bulletsecond = Instantiate(bullet2, gunPosition.position, Quaternion.identity);
                Rigidbody2D rb = bulletsecond.GetComponent<Rigidbody2D>();
                rb.AddForce(gunPosition.up * 10,ForceMode2D.Impulse);
                ratefire = false;
                yield return new WaitForSeconds(4);
                ratefire = true;
            }
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(PlayerDeath, Camera.main.transform.position, PlayerDeathVolume);
        }
    }
}