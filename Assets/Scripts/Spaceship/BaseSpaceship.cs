using UnityEngine;
using  System;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected Bullet bullet2;
        public event Action Exploded;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }
        public Bullet Bullet2 { get; private set; }

        protected void Init(int hp, float speed, Bullet bullet , Bullet bullet2)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
            Bullet2 = bullet2;
        }

        public virtual void Fire()
        {
            
        }

        public virtual void Fire2()
        {
            
        }
    }
}